const express=require('express');
const path=require('path')
require("dotenv").config();
const cors=require('cors');
const mongoose=require('mongoose');
const cookieParser=require('cookie-parser');
mongoose.connect("mongodb+srv://adarsh-admin:AoUJo2luTwjrCDHv@cluster0.jjs5s.mongodb.net/doubt");
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'Mongodb Connection Error:'));
db.once('open', () => {
     console.log('Mongodb Connection Successful');
});
const app=express();
app.use(express.static(__dirname+'/client/build'));
const PORT=process.env.PORT || 8080;
app.use(express.json());
app.use(cookieParser());
app.use(express.urlencoded({extended:true}));
// app.use(express.static(__dirname+'/public'));
 app.use(cors({credentials:true,origin:"*"}))
const router = require("./routes/api");
app.use("/api", router);


app.get('/*',(req,res)=>{
  res.sendFile(path.join(__dirname+'/client/build/index.html'))
})
app.listen(PORT,()=>{
    console.log(`server is running on port ${PORT}`);
});
