import React from "react";
import {
  NavbarWrapper,
  FormGroup,
  Label,
  Form,
  Title,
  Container,
  AuthContainer,
  Input,
} from "./LandingElements";
import { useDispatch, useSelector } from "react-redux";
import Navbar from "../../components/Navbar/index";
import { useNavigate } from "react-router-dom";
import { Swal } from "sweetalert2";
function Landing() {
  let info = "for student uid>=7 ,for teacher uid<=5 ,for ta/moderator uid==6";
  const select = useSelector((state) => state.authReducer);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [login, setLogin] = React.useState(false);
  if (select.isTeacher || select.isStudent || select.isModerator) {
    navigate("home");
  }
  const toggleLogin = () => {
    setLogin(!login);
  };
  const [loginData, setLoginData] = React.useState({
    email: "",
    password: "",
  });
  const [loginErr,setLoginerr]=React.useState({
    email:"",
    password:""
  })
  const handleChange = (e) => {
    //   console.log({[e.target.name]:e.target.value});
    if (e.target.name === "email") {
      if(!validateEmail(e.target.value) || e.target.value.length===0){
        setLoginerr({...loginErr,email:"invalid email"})
      }
      else{
        setLoginerr({...loginErr,email:""})
      }
    }else{
      if(e.target.value.length===0){
        setLoginerr({...loginErr,password:"invalid password"})
      }
      else{
        setLoginerr({...loginErr,password:""})
      }
    }
    setLoginData({ ...loginData, [e.target.name]: e.target.value });
    
  };
  const handleSubmitLogin = (e) => {
    e.preventDefault();
    console.log(loginData);
    if(loginData.email==="" && !validateEmail(loginData.email)){
      setLoginerr({...loginErr,email:"Please enter a valid email"})
      return;
    }
    if(loginData.password===""){
      setLoginerr({...loginErr,password:"Please enter a password"})
      return;
    }
    dispatch({ type: "LOGIN", payload: loginData });
  };
  const [signupData, setSignupData] = React.useState({
    name: "",
    email: "",
    password: "",
    confirmpassword: "",
    uid: "",
  });
  const [role,setRole] = React.useState("student")
  const [err,setErr]=React.useState({
    name:"",
    email:"",
    password:"",
    confirmpassword:"",
    uid:"",
  })
  function validateEmail(email) 
    {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }
  const handleSignupChange = (e) => {
    if(e.target.value===""){
      setErr({...err,[e.target.name]:"field cannot be empty"})
    }
    if(e.target.name==="uid"){
      if(role==="student"){
        if(e.target.value.length<7 || e.target.value.length===0){
          setErr({...err,[e.target.name]:"uid should be greater than 7"})
        }
        else{
          setErr({...err,[e.target.name]:""})
        }
      }
      else if(role==="teacher"){
        if(e.target.value.length>5 || e.target.value.length===0){
          setErr({...err,[e.target.name]:"uid should be less than 5"})
        }
        else{
          setErr({...err,[e.target.name]:""})
        }
      }
      else if(role==="moderator" || e.target.value.length===0){
        if(e.target.value.length!==6){
          setErr({...err,[e.target.name]:"uid should be 6"})
        }
        else{
          setErr({...err,[e.target.name]:""})
        }
      }
    }
    if(e.target.name==="name"){
      if(e.target.value.length===0){
        setErr({...err,[e.target.name]:"name cannot be empty"})
      }
      else{
        setErr({...err,[e.target.name]:""})
      }
    }
    if(e.target.name==="email"){
      validateEmail(e.target.value)?setErr({...err,[e.target.name]:""}):setErr({...err,[e.target.name]:"invalid email"})
    }
    
    if(e.target.name==="confirmpassword"){
      if(e.target.value===signupData.password){
        setErr({...err,[e.target.name]:""})
      }
      else{
        setErr({...err,[e.target.name]:"password doesn't match"})
      }
    }
    console.log(signupData, "uhikhu");
    setSignupData({ ...signupData, [e.target.name]: e.target.value });
  };
  const handleSubmitSignup = () => {
    dispatch({ type: "SIGNUP", payload: signupData });
  };

  return (
    <Container>
      
      {login && (
        <AuthContainer>
          <Title>Login</Title>
          <Form>
            <FormGroup>
              <Label>Email</Label>
              <Input
                type="email"
                placeholder="Enter your email"
                className="text-center p-1"
                onChange={handleChange}
                name="email"
                value={loginData.email}
              />
              {loginErr.email!=="" && <p className="text-danger">{loginErr.email}</p>}
            </FormGroup>
            <FormGroup>
              <Label>Password</Label>
              <Input
                type="password"
                className="formControl text-center p-1"
                placeholder="Enter your password"
                onChange={handleChange}
                name="password"
                value={loginData.password}
              />
              {loginErr.password!=="" && <p className="text-danger">{loginErr.password}</p>}
            </FormGroup>
            <FormGroup>
              <button className="btn btn-secondary" onClick={handleSubmitLogin}>
                login
              </button>
              <p>have no account ?</p>
              <button onClick={toggleLogin} class="btn btn-dark">
                signup
              </button>
            </FormGroup>
          </Form>
        </AuthContainer>
      )}
      {!login && (
        <AuthContainer>
          <Title>Signup</Title>
         
          <Form>
            <FormGroup>
              <Label>Choose role</Label>
              <select name="role" onChange={(e)=>{
                setRole(e.target.value)
              }} value={role}>
                <option value="student">student</option>
                <option value="teacher">teacher</option>
                <option value="moderator">moderator</option>
              </select>
              <Label>UID</Label>
              <Input
                type="text"
                className="text-center"
                placeholder="enter your userid"
                onChange={handleSignupChange}
                name="uid"
                value={signupData.uid}
              />
              {err.uid!==""&&<p style={{color:"red"}}>{err.uid}</p>}
            </FormGroup>
            <FormGroup>
              <Label>Name</Label>
              <Input
                type="text"
                placeholder="Enter your name"
                className="text-center p-1"
                onChange={handleSignupChange}
                name="name"
                value={signupData.name}
              />
              {err.name!==""&&<p className="text-danger">{err.name}</p>}
            </FormGroup>
            <FormGroup>
              <Label>Email</Label>
              <Input
                type="email"
                placeholder="Enter your email"
                className="text-center p-1"
                onChange={handleSignupChange}
                name="email"
                value={signupData.email}
              />
              {err.email===""?"":<p className="text-danger">{err.email}</p>}
            </FormGroup>
            <FormGroup>
              <Label>Password</Label>
              <Input
                type="password"
                className="text-center p-1"
                placeholder="Enter your password"
                onChange={handleSignupChange}
                name="password"
                value={signupData.password}
              />
              {err.password!==""&&<p className="text-danger">{err.password}</p>}
            </FormGroup>
            <FormGroup>
              <Label>Confirm Password</Label>
              <Input
                type="password"
                required
                className="text-center p-1"
                placeholder="Enter your password"
                onChange={handleSignupChange}
                name="confirmpassword"
                value={signupData.confirmpassword}
              />
            </FormGroup>
            {err.confirmpassword!==""&&<p className="text-danger">{err.confirmpassword}</p>}
            <FormGroup>
              <button
                className="btn btn-secondary"
                onClick={(e) => {
                  e.preventDefault();
                  let flag=false;
                  if(role==="student" && signupData.uid.length<7){
                    flag=true;
                    setErr({...err,uid:"length should be more or equal to 7"})
                  }
                  else if(role==="teacher" && signupData.uid.length<=5){
                    flag=true;
                    setErr({...err,uid:"length is 5 or less"})
                  }
                  else if(role==="moderator" && signupData.uid.length!==6){
                    flag=true;
                    setErr({...err,uid:"length is 6 digit"})
                  }
                  else if(signupData.name===""){
                    flag=true;
                    setErr({...err,name:"name is required"})
                  }
                  else if(signupData.email==="" || !validateEmail(signupData.email)){
                    flag=true;
                    setErr({...err,email:"email is required"})
                  }
                  else if(signupData.password===""){
                    flag=true;
                    setErr({...err,password:"password is required"})
                  }
                  else if(signupData.confirmpassword===""){
                    flag=true;
                    setErr({...err,confirmpassword:"confirm password is required"})
                  }
                  else if(signupData.password!==signupData.confirmpassword){
                    flag=true;
                    setErr({...err,confirmpassword:"password and confirm password should be same"})
                  }
                  else {
                  dispatch({ type: "REGISTER", payload: signupData });
                  setLogin(true);
                  }
                }}
              >
                signup
              </button>
              <p>have an account ?</p>
              <button onClick={toggleLogin} className="btn btn-dark">
                login
              </button>
            </FormGroup>
          </Form>
        </AuthContainer>
      )}
    </Container>
  );
}

export default Landing;
