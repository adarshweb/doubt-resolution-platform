### [https://doubtresolution.herokuapp.com/](https://doubtresolution.herokuapp.com/)

#### This portal helps in bridging the gap between the students and teachers. A community of learners can use this to benefit among them.

### MERN Stack
## Features
- User authentication/authorization and email verification
- Three roles/portals namely admin, moderator and student.
- Students can raise doubt, other students can see the doubt and comment on them
- Moderators can accept and solve or escalate the doubt, time stamp will be recorded
- Teaching admins can see the overall report of the doubts in the platform, average time taken to resolve, and much more.
- Teaching admins can also see reports generated for each of the moderator in the system.

## Schema design

    user :{
        unique id
        isTeacher/isModerator/isStudent 
        name
        email
        verified
        password
    }

    Doubt :{
        question
        raisedBy 
        comments : list of comments
        time: at which doubt was created
        resolved: true/false
        answer,answerTime
        accepted: is the question accepted by someone
    }

    This is for generating reports
    Moderators:{
        unique id
        doubt id
        resolved : true/false
        time stamp for acceptance
        time stamp for resolved
    }

### Tools used in frontend
    - Redux-toolkit
    - Redux-saga
    - Styled-components
